# 本地實體安裝模式

## 系統設定

### 遠端連線設定：

本步驟旨在於設定遠端連線（SSH）服務，以利異地進行佈署和操作。

#### 準備材料：已完成網路設定之樹莓派及其周邊設備、個人電腦（須連上網路）

1. 於個人電腦上開啟終端機
    > + Windows 系統為「PowerShell」或「命令提示字符」，另外 Windows 10 系統亦可於微軟商店下載 Windows Terminal 進行使用。
    > + Mac 系統可由系統工具或 Spotlight 找到終端機。
    > + Linux 系統請開啟相應之命令行工具。

2. 輸入 `ssh ubuntu@<ip address>`，並將 `<ip address>` 替換為網路設定步驟樹梅派之 ip 位址，範例如下
    ```console
    C:\Users\user> ssh ubuntu@192.168.10.1
    ```
    > 註：`C:\Users\user>` 為 Windows 系統命令提示字符當前目錄之開頭，指令由此段文字之後開始。
3. 若連線成功，則應顯示如下之提示，並於此時輸入帳戶之密碼，按下 `Enter`
    ```console
    ubuntu@192.168.10.1's password:
    ```
4. 若登入成功，則應看到熟悉之起始畫面
    ```console
    Welcome to Ubuntu 20.04 LTS (GNU/Linux 5.4.0-1008-raspi aarch64)

    * Documentation:  https://help.ubuntu.com
    * Management:     https://landscape.canonical.com
    * Support:        https://ubuntu.com/advantage

    System information as of Fri Jul 10 07:24:58 UTC 2020

    System load:  0.0               Processes:              131
    Usage of /:   8.5% of 29.17GB   Users logged in:        0
    Memory usage: 9%                IPv4 address for eth0:  10.10.8.110
    Swap usage:   0%                IPv4 address for wlan0: 192.168.10.1
    Temperature:  47.2 C

    * "If you've been waiting for the perfect Kubernetes dev solution for
    macOS, the wait is over. Learn how to install Microk8s on macOS."

    https://www.techrepublic.com/article/how-to-install-microk8s-on-macos/

    52 updates can be installed immediately.
    0 of these updates are security updates.
    To see these additional updates run: apt list --upgradable


    *** System restart required ***
    Last login: Fri Jul 10 06:11:02 2020 from 192.168.10.101
    ubuntu@ubuntu:~$
    ```
5. 此時已經可知道 SSH 連線正常運作，未來便可以使用此方式進行遠端操作。
6. 輸入指令 `logout` 先登出系統（註：輸入指令之方式為輸入指令文字，並按下 `Enter` 鍵）
7. 於終端機輸入如下指令，為了操作方便，本步驟所有提示皆按 `Enter` 即可
    ```
    C:\Users\user> ssh-keygen -b 4096
    ```
    詳細操作方式可參考官方文件 https://www.ssh.com/ssh/keygen/
8. 將第 7. 步所產生之公鑰上傳至伺服器（請注意保存好私鑰，且不得公開，否則他人將可任意入侵）。  
    </br>
    Windows 系統：先切換至使用者目錄底下，並按下列程序輸入指令
    ```console
    C:\Users\user> cat ~/.ssh/id_rsa.pub | ssh ubuntu@<樹莓派ip> 'cat >> ~/.ssh/authorized_keys'
    
    ubuntu@<樹莓派ip>'s password: #輸入ubuntu帳戶密碼

    C:\Users\user> ssh ubuntu@<樹莓派ip>

    ubuntu@<樹莓派ip>'s password: #輸入ubuntu帳戶密碼

    ubuntu@ubuntu:~$ chmod 700 ~/.ssh

    ubuntu@ubuntu:~$ chmod 644 ~/.ssh/authorized_keys
    ```
    Mac 或 Linux 系統：輸入下列指令，並依提示輸入密碼即可
    ```console
    user@manjaro-pc:~$ ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@<樹莓派ip>
    ```
9.  以 SSH 方式登入樹莓派，並輸入下面指令
    ```console
    ubuntu@ubuntu:~$ sudo nano /etc/ssh/sshd_config
    ```
10. 將 `#PubkeyAuthentication yes` 剛行之 `#` 註解移除
11. 按下 `ctrl` + `X` 鍵，之後按下 `Y` 鍵選擇保存，再按下 `Enter`（此為 nano 編輯器覆寫原檔案之保存方式，請牢記）
12. 輸入 `sudo systemctl restart sshd` 並依提示輸入密碼，再登出系統
13. 此時於終端機輸入 `ssh -i .ssh/id_rsa ubuntu@<樹莓派ip>` 應該可以成功登入
14. 成功登入後，再次重複步驟 9. 並將下列該行之 `#` 註解移除和修正文字(以小括弧表示)
    ```
    Port 22 (將 22 替換為其他 1000-9000 之數字，並記下此「連接埠號」)
    LogLevel VERBOSE
    PermitRootLogin no
    PasswordAuthentication no
    PermitEmptyPasswords no
    X11Forwarding no
    #文件底端增加
    Protocol 2
    ```
15. 輸入 `sudo systemctl restart sshd` 並依提示輸入密碼，再登出系統
16. 往後以如下指令進行無密碼登入，可確保安全性
    ```console
    C:\Users\user> ssh -i .ssh/id_rsa -p <連接埠號> ubuntu@<樹莓派ip>
    ```
17. （可選）自行測試以步驟 16. 前之 SSH 指令登入，應無法登入 
18. 此時已可以移除鍵盤、螢幕，僅以遠端進行操作。