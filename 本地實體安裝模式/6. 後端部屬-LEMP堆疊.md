# 本地實體安裝模式

## 後端部署

### LEMP堆疊：

本步驟旨在於部署由 Linux、Nginx、MySQL、Php 所組成之堆疊，以建立伺服器可用之後端。

#### 準備材料：已完成防火牆設定之樹莓派、個人電腦（須連上網路）

1. 以 SSH 連線登入樹莓派伺服器
2. 先輸入 `sudo apt upgrade` 進行系統更新
3. 輸入 `sudo apt install nginx`，此時可以看到類似如下之提示（僅截取部分）
    ```console
    Reading package lists... Done
    Building dependency tree
    Reading state information... Done
    The following additional packages will be installed:
      fontconfig-config fonts-dejavu-core libfontconfig1 libfreetype6 libgd3 libjbig0 libjpeg-turbo8 libjpeg8
      libnginx-mod-http-image-filter libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-stream libtiff5 libwebp6
      libxpm4 libxslt1.1 nginx-common nginx-core
    Suggested packages:
      libgd-tools fcgiwrap nginx-doc ssl-cert
    The following NEW packages will be installed:
      fontconfig-config fonts-dejavu-core libfontconfig1 libfreetype6 libgd3 libjbig0 libjpeg-turbo8 libjpeg8
      libnginx-mod-http-image-filter libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-stream libtiff5 libwebp6
    ```
4. 此信息為系統提示將會安裝之套件，並在看到如下信息處輸入 `Y` 以繼續安裝。
    ```console
    Do you want to continue? [Y/n]
    ```
5. 輸入 `sudo ufw allow 'Nginx FULL'` 來將防火牆之 80、443 網頁伺服器端口打開
6. 打開網頁瀏覽器，輸入伺服器ip位址，應可見如下畫面<img src="https://cdn.wp.nginx.com/wp-content/uploads/2014/01/welcome-screen-e1450116630667.png" style="zoom:70%" />
7. 輸入 `sudo apt install mysql-server` 並重複步驟 4 之操作以安裝 MySQL 資料庫伺服器
8. 輸入下方指令，以進行 MySQL Server 之設定
    ```console
    ubuntu@ubuntu:~$ sudo mysql_secure_installation
    ```
9. 此時應可見如下提示，爲設定密碼強度檢查插件，輸入 `y` 並按下確認則表示使用該插件，若不想使用，則點按其他按鍵即可略過
    ```console
    Securing the MySQL server deployment.

    Connecting to MySQL using a blank password.

    VALIDATE PASSWORD COMPONENT can be used to test passwords
    and improve security. It checks the strength of password
    and allows the users to set only those passwords which are
    secure enough. Would you like to setup VALIDATE PASSWORD component?

    Press y|Y for Yes, any other key for No: y
    ```
    若輸入 `y` 則應可見如下提示，0、1、2 分別表示不同強度的密碼檢查，本例則使用 0 即最低強度的密碼檢查。
    ```console
    There are three levels of password validation policy:

    LOW    Length >= 8
    MEDIUM Length >= 8, numeric, mixed case, and special characters
    STRONG Length >= 8, numeric, mixed case, special characters and dictionary                  file

    Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 0
    ```
10. 確認後，系統將提示設定 MySQL 服務之 Root 密碼，此密碼不同於系統之 Root 密碼，請審慎設定並牢記之。
    ```console
    Please set the password for root here.

    New password:

    Re-enter new password:
    ```
    待重複輸入並確認欲設定之密碼無誤後，系統將再次詢問是否要以剛才輸入之密碼進行設定，若確認則輸入 `y`。
    ```console
    Estimated strength of the password: 50
    Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y
    ```
11. 之後系統將提示更多安全性之設定，爲保證伺服器運作之安全性，後續提示皆輸入 `y` 即可。
    ```console
    By default, a MySQL installation has an anonymous user,
    allowing anyone to log into MySQL without having to have
    a user account created for them. This is intended only for
    testing, and to make the installation go a bit smoother.
    You should remove them before moving into a production
    environment.

    Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
    Success.


    Normally, root should only be allowed to connect from
    'localhost'. This ensures that someone cannot guess at
    the root password from the network.

    Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
    Success.

    By default, MySQL comes with a database named 'test' that
    anyone can access. This is also intended only for testing,
    and should be removed before moving into a production
    environment.


    Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y
     - Dropping test database...
    Success.

     - Removing privileges on test database...
    Success.

    Reloading the privilege tables will ensure that all changes
    made so far will take effect immediately.

    Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
    Success.

    All done!
    ```
    更多參數資訊可參考官方說明文件 [4.4.2 mysql_secure_installation — Improve MySQL Installation Security](https://dev.mysql.com/doc/refman/8.0/en/mysql-secure-installation.html)
12. 輸入 `sudo apt install php-fpm php-mysql` 以安裝 PHP
13. 可參考之教學文件 [How To Install Linux, Nginx, MySQL, PHP (LEMP stack) on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-ubuntu-20-04)
